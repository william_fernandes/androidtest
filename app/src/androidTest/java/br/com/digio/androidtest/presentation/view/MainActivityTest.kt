package br.com.digio.androidtest.presentation.view

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import br.com.digio.androidtest.R
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun useAppContext() {
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        Assert.assertEquals("br.com.digio.androidtest", appContext.packageName)
    }

    @Test
    fun checkProductListVisible() {
        onView(withId(R.id.recyMainProducts))
            .check(matches(isDisplayed()))

        onView(withId(R.id.recyMainProducts))
            .check(matches(hasMinimumChildCount(1)))
    }

    @Test
    fun checkSpotlightListVisible() {
        onView(withId(R.id.recyMainSpotlight))
            .check(matches(isDisplayed()))

        onView(withId(R.id.recyMainSpotlight))
            .check(matches(hasMinimumChildCount(1)))
    }

    @Test
    fun testActivityDisplayed() {
        onView(withId(R.id.scrollable)).check(matches(isDisplayed()))
        onView(withId(R.id.title)).check(matches(withText(R.string.hello_maria)))
        onView(withId(R.id.recyMainSpotlight)).check(matches(isDisplayed()))
        onView(withId(R.id.recyMainProducts)).check(matches(isDisplayed()))
    }

    @Test
    fun verifyProductsListIsNotEmpty() {
        onView(withId(R.id.recyMainProducts)).check(matches(hasMinimumChildCount(1)))
    }

    @Test
    fun verifySpotlightsListIsNotEmpty() {
        onView(withId(R.id.recyMainSpotlight)).check(matches(hasMinimumChildCount(1)))
    }
}
