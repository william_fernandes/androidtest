package br.com.digio.androidtest.presentation.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<T, VH : RecyclerView.ViewHolder>(
    private val layoutResId: Int,
    private val bindViewHolder: (VH, T) -> Unit,
) : RecyclerView.Adapter<VH>() {

    var items: List<T> = emptyList()
        set(value) {
            val result = DiffUtil.calculateDiff(
                BaseListDiffCallback(
                    field,
                    value,
                ),
            )
            result.dispatchUpdatesTo(this)
            field = value
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val view = LayoutInflater.from(parent.context).inflate(
            layoutResId,
            parent,
            false,
        )
        return createViewHolder(view)
    }

    abstract fun createViewHolder(view: View): VH

    override fun onBindViewHolder(holder: VH, position: Int) {
        bindViewHolder(holder, items[position])
    }

    override fun getItemCount(): Int = items.size
}
