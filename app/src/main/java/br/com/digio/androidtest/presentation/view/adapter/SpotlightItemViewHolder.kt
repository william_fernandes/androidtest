package br.com.digio.androidtest.presentation.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.androidtest.R
import br.com.digio.androidtest.data.model.Spotlight
import br.com.digio.androidtest.databinding.ItemMainSpotlightBinding
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class SpotlightItemViewHolder(private val binding: ItemMainSpotlightBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(product: Spotlight) {
        binding.apply {
            binding.imgMainItem.contentDescription = product.name
            binding.progressImage.visibility = View.VISIBLE

            Picasso.get()
                .load(product.bannerURL)
                .error(R.drawable.ic_alert_circle)
                .into(
                    binding.imgMainItem,
                    object : Callback {
                        override fun onSuccess() {
                            binding.progressImage.visibility = View.GONE
                        }

                        override fun onError(e: Exception?) {
                            binding.progressImage.visibility = View.GONE
                        }
                    },
                )
        }
    }
}
