package br.com.digio.androidtest.presentation.view

import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.androidtest.DigioApp
import br.com.digio.androidtest.R
import br.com.digio.androidtest.data.model.Cash
import br.com.digio.androidtest.data.repository.ErrorType
import br.com.digio.androidtest.data.repository.ViewState
import br.com.digio.androidtest.databinding.ActivityMainBinding
import br.com.digio.androidtest.presentation.view.adapter.ProductAdapter
import br.com.digio.androidtest.presentation.view.adapter.SpotlightAdapter
import br.com.digio.androidtest.presentation.viewmodel.CashViewModel
import br.com.digio.androidtest.util.ErrorUtils
import br.com.digio.androidtest.util.NetworkUtils.isNetworkAvailable
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var cashViewModel: CashViewModel

    private lateinit var binding: ActivityMainBinding

    private val productAdapter: ProductAdapter by lazy {
        ProductAdapter()
    }

    private val spotlightAdapter: SpotlightAdapter by lazy {
        SpotlightAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (application as DigioApp).appComponent.inject(this)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        observeViewModel()

        if (!isNetworkAvailable(this@MainActivity)) {
            Toast.makeText(
                this,
                ErrorUtils.handleError(this@MainActivity, ErrorType.NETWORK_ERROR),
                Toast.LENGTH_SHORT,
            ).show()
        }
    }

    private fun initView() {
        binding.recyMainProducts.apply {
            layoutManager = LinearLayoutManager(
                this@MainActivity,
                LinearLayoutManager.HORIZONTAL,
                false,
            )
            adapter = productAdapter
        }

        binding.recyMainSpotlight.apply {
            layoutManager = LinearLayoutManager(
                this@MainActivity,
                LinearLayoutManager.HORIZONTAL,
                false,
            )
            adapter = spotlightAdapter
        }
    }

    private fun observeViewModel() {
        lifecycleScope.launch {
            cashViewModel.cash.collectLatest { state ->
                when (state) {
                    is ViewState.Loading -> {
                        binding.loadDigioContainer.root.visibility = View.VISIBLE
                    }
                    is ViewState.Success -> {
                        binding.loadDigioContainer.root.visibility = View.GONE
                        state.data?.let { setupDigioCashText(it) }
                    }
                    is ViewState.Failed -> {
                        binding.loadDigioContainer.root.visibility = View.GONE
                        ErrorUtils.handleError(this@MainActivity, ErrorType.GENERIC_ERROR)
                        state.throwable.printStackTrace()
                    }
                    else -> {}
                }
            }
        }

        lifecycleScope.launch {
            cashViewModel.products.collectLatest { state ->
                when (state) {
                    is ViewState.Loading -> {
                        binding.loadDigioContainer.root.visibility = View.VISIBLE
                    }
                    is ViewState.Success -> {
                        binding.loadDigioContainer.root.visibility = View.GONE
                        state.data?.let { products ->
                            productAdapter.items = products
                        }
                    }
                    is ViewState.Failed -> {
                        binding.loadDigioContainer.root.visibility = View.GONE
                        ErrorUtils.handleError(this@MainActivity, ErrorType.GENERIC_ERROR)
                        state.throwable.printStackTrace()
                    }
                    else -> {}
                }
            }
        }

        lifecycleScope.launch {
            cashViewModel.spotlights.collectLatest { state ->
                when (state) {
                    is ViewState.Loading -> {
                        binding.loadDigioContainer.root.visibility = View.VISIBLE
                    }
                    is ViewState.Success -> {
                        binding.loadDigioContainer.root.visibility = View.GONE
                        state.data?.let { spotlights ->
                            spotlightAdapter.items = spotlights
                        }
                    }
                    is ViewState.Failed -> {
                        binding.loadDigioContainer.root.visibility = View.GONE
                        ErrorUtils.handleError(this@MainActivity, ErrorType.GENERIC_ERROR)
                        state.throwable.printStackTrace()
                    }
                    else -> {}
                }
            }
        }
    }

    private fun setupDigioCashText(cash: Cash) {
        val digioCacheText = "${cash.title} ${getString(R.string.digio_cache)}"
        binding.txtMainDigioCash.text = SpannableString(digioCacheText).apply {
            cash.title?.length?.let { title ->
                setSpan(
                    ForegroundColorSpan(ContextCompat.getColor(this@MainActivity, R.color.blue_darker)),
                    0,
                    title,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE,
                )
                setSpan(
                    ForegroundColorSpan(
                        ContextCompat.getColor(
                            this@MainActivity,
                            R.color.font_color_digio_cash,
                        ),
                    ),
                    cash.title.length + 1,
                    digioCacheText.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE,
                )
            }
        }
    }
}