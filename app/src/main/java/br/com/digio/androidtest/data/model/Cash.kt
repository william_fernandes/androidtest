package br.com.digio.androidtest.data.model

data class Cash(
    val bannerURL: String? = "",
    val title: String? = "",
)
