package br.com.digio.androidtest.presentation.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import br.com.digio.androidtest.R
import br.com.digio.androidtest.data.model.Product
import br.com.digio.androidtest.databinding.ItemMainProductsBinding
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class ProductItemViewHolder(
    private val binding: ItemMainProductsBinding,
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(product: Product) {
        binding.apply {
            binding.imgMainItem.contentDescription = product.name
            binding.progressImage.visibility = View.VISIBLE

            Picasso.get()
                .load(product.imageURL)
                .error(R.drawable.ic_alert_circle)
                .into(
                    binding.imgMainItem,
                    object : Callback {
                        override fun onSuccess() {
                            binding.progressImage.visibility = View.GONE
                        }

                        override fun onError(e: Exception?) {
                            binding.progressImage.visibility = View.GONE
                        }
                    },
                )
        }
    }
}
