package br.com.digio.androidtest.data.remote

import br.com.digio.androidtest.data.model.DigioProducts
import retrofit2.Call
import retrofit2.http.GET

interface DigioEndpoint {
    @GET("sandbox/products")
    fun getProducts(): Call<DigioProducts>
}