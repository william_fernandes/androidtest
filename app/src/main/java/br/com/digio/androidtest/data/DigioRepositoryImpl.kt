package br.com.digio.androidtest.data

import br.com.digio.androidtest.data.local.dao.ProductDao
import br.com.digio.androidtest.data.local.dao.SpotlightDao
import br.com.digio.androidtest.data.model.Cash
import br.com.digio.androidtest.data.model.DigioProducts
import br.com.digio.androidtest.data.remote.DigioEndpoint
import br.com.digio.androidtest.data.repository.DigioRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DigioRepositoryImpl(
    private val endpoint: DigioEndpoint,
    private val productDao: ProductDao,
    private val spotlightDao: SpotlightDao,
) : DigioRepository {

    override suspend fun getProducts(): DigioProducts? {
        return try {
            val data = withContext(Dispatchers.IO) {
                endpoint.getProducts().execute().body()
            }

            data?.let {
                deleteAllProducts()
                saveToDatabase(it)
                it
            }
        } catch (e: Exception) {
            getCachedProducts()
        }
    }

    private suspend fun getCachedProducts(): DigioProducts? {
        val cachedProducts = productDao.getProducts()
        val cachedSpotlights = spotlightDao.getSpotlights()

        return cachedProducts.let { products ->
            cachedSpotlights.let { spotlights ->
                if (products.isNotEmpty() && spotlights.isNotEmpty()) {
                    DigioProducts(Cash("", ""), products, spotlights)
                } else {
                    null
                }
            }
        }
    }

    private suspend fun deleteAllProducts() {
        withContext(Dispatchers.IO) {
            productDao.dropTableProduct()
            spotlightDao.dropTableSpotlight()
        }
    }

    internal suspend fun saveToDatabase(data: DigioProducts) {
        withContext(Dispatchers.IO) {
            productDao.insertAll(data.products)
            spotlightDao.insertAll(data.spotlight)
        }
    }
}
