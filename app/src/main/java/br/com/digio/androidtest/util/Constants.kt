package br.com.digio.androidtest.util

interface Constants {

    companion object {
        const val ENDPOINT_URL = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/"
        const val DATABASE_NAME = "digio.db"
    }
}
