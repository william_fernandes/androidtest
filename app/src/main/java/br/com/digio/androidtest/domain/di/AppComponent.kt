package br.com.digio.androidtest.domain.di

import br.com.digio.androidtest.presentation.view.MainActivity
import dagger.Component

@Component(modules = [DigioModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
}
