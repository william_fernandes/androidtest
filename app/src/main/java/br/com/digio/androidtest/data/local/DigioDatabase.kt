package br.com.digio.androidtest.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.com.digio.androidtest.data.local.dao.ProductDao
import br.com.digio.androidtest.data.local.dao.SpotlightDao
import br.com.digio.androidtest.data.model.Product
import br.com.digio.androidtest.data.model.Spotlight
import br.com.digio.androidtest.util.Constants.Companion.DATABASE_NAME

@Database(
    entities = [Product::class, Spotlight::class],
    version = 1,
    exportSchema = false,
)
abstract class DigioDatabase : RoomDatabase() {

    abstract fun productDao(): ProductDao

    abstract fun spotlightDao(): SpotlightDao

    companion object {

        @Volatile
        private var instance: DigioDatabase? = null

        fun getInstance(context: Context): DigioDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): DigioDatabase {
            return Room.databaseBuilder(context, DigioDatabase::class.java, DATABASE_NAME)
                .build()
        }
    }
}
