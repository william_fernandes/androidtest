package br.com.digio.androidtest.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "spotlight")
data class Spotlight(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,

    @ColumnInfo(name = "banner_url")
    var bannerURL: String? = "",

    @ColumnInfo(name = "name")
    var name: String? = "",

    @ColumnInfo(name = "description")
    var description: String? = "",
) {
    constructor() : this(0, "", "", "")

    @Ignore
    constructor(bannerURL: String?, name: String?, description: String?) : this(0, bannerURL, name, description)
}
