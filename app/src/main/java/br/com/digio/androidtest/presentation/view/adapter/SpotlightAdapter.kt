package br.com.digio.androidtest.presentation.view.adapter

import android.view.View
import br.com.digio.androidtest.R
import br.com.digio.androidtest.data.model.Spotlight
import br.com.digio.androidtest.databinding.ItemMainSpotlightBinding

class SpotlightAdapter : BaseAdapter<Spotlight, SpotlightItemViewHolder>(
    R.layout.item_main_spotlight,
    { vh, item -> vh.bind(item) },
) {
    override fun createViewHolder(view: View): SpotlightItemViewHolder =
        SpotlightItemViewHolder(ItemMainSpotlightBinding.bind(view))
}