package br.com.digio.androidtest

import android.app.Application
import br.com.digio.androidtest.domain.di.AppComponent
import br.com.digio.androidtest.domain.di.DaggerAppComponent
import br.com.digio.androidtest.domain.di.DigioModule

class DigioApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .digioModule(DigioModule(applicationContext))
            .build()
    }
}
