package br.com.digio.androidtest.presentation.view.adapter

import android.view.View
import br.com.digio.androidtest.R
import br.com.digio.androidtest.data.model.Product
import br.com.digio.androidtest.databinding.ItemMainProductsBinding

class ProductAdapter : BaseAdapter<Product, ProductItemViewHolder>(
    R.layout.item_main_products,
    { vh, item -> vh.bind(item) },
) {
    override fun createViewHolder(view: View): ProductItemViewHolder =
        ProductItemViewHolder(ItemMainProductsBinding.bind(view))
}