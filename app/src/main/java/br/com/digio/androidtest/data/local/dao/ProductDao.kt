package br.com.digio.androidtest.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.digio.androidtest.data.model.Product

@Dao
interface ProductDao {

    @Query("SELECT * FROM product")
    suspend fun getProducts(): List<Product>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(products: List<Product>)

    @Query("DELETE FROM product")
    fun dropTableProduct()
}
