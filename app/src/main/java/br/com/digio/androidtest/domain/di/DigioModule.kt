package br.com.digio.androidtest.domain.di

import android.content.Context
import androidx.room.Room
import br.com.digio.androidtest.data.DigioRepositoryImpl
import br.com.digio.androidtest.data.local.DigioDatabase
import br.com.digio.androidtest.data.local.dao.ProductDao
import br.com.digio.androidtest.data.local.dao.SpotlightDao
import br.com.digio.androidtest.data.remote.DigioEndpoint
import br.com.digio.androidtest.data.repository.DigioRepository
import br.com.digio.androidtest.domain.usecases.ProductUseCase
import br.com.digio.androidtest.util.Constants.Companion.DATABASE_NAME
import br.com.digio.androidtest.util.Constants.Companion.ENDPOINT_URL
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class DigioModule(private val context: Context) {
    @Provides
    fun provideContext(): Context = context

    @Provides
    fun provideEndpoint(): DigioEndpoint {
        val gson: Gson = GsonBuilder().create()

        val cacheInterceptor = CacheInterceptor(context)
        val okHttp: OkHttpClient = OkHttpClient.Builder()
            .cache(cacheInterceptor.cache)
            .addInterceptor(cacheInterceptor)
            .build()

        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(ENDPOINT_URL)
            .client(okHttp)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        return retrofit.create(DigioEndpoint::class.java)
    }

    @Provides
    fun provideDatabase(context: Context): DigioDatabase {
        return Room.databaseBuilder(
            context,
            DigioDatabase::class.java,
            DATABASE_NAME,
        ).build()
    }

    @Provides
    fun provideProductDao(database: DigioDatabase): ProductDao {
        return database.productDao()
    }

    @Provides
    fun provideSpotlightDao(database: DigioDatabase): SpotlightDao {
        return database.spotlightDao()
    }

    @Provides
    fun provideRepository(endpoint: DigioEndpoint, productDao: ProductDao, spotlightDao: SpotlightDao): DigioRepository {
        return DigioRepositoryImpl(endpoint, productDao, spotlightDao)
    }

    @Provides
    fun provideProductUseCase(repository: DigioRepository): ProductUseCase {
        return ProductUseCase(repository)
    }
}
