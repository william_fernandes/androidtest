package br.com.digio.androidtest.domain.di

import android.content.Context
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.Response
import java.io.File
import java.io.IOException

class CacheInterceptor(context: Context) : Interceptor {

    private val cacheSize = 5 * 1024 * 1024 // 5 MB
    private val cacheDirectoryName = "responses_cache"
    private val cacheDirectory = File(context.cacheDir, cacheDirectoryName)
    val cache = Cache(cacheDirectory, cacheSize.toLong())

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()

        request = if (request.header("Cache-Control") == null) {
            request.newBuilder()
                .header("Cache-Control", "public, max-age=" + 60)
                .build()
        } else {
            request.newBuilder().build()
        }

        val response = chain.proceed(request)
        return if (response.code == 504) {
            response.newBuilder().build()
        } else {
            response.newBuilder()
                .header("Cache-Control", "public, max-age=" + 60)
                .build()
        }
    }
}
