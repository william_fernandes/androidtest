package br.com.digio.androidtest.data.repository

import br.com.digio.androidtest.data.model.DigioProducts

interface DigioRepository {
    suspend fun getProducts(): DigioProducts?
}
