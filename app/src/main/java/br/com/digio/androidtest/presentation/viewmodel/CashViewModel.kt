package br.com.digio.androidtest.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.digio.androidtest.data.model.Cash
import br.com.digio.androidtest.data.model.Product
import br.com.digio.androidtest.data.model.Spotlight
import br.com.digio.androidtest.data.repository.ViewState
import br.com.digio.androidtest.domain.usecases.ProductUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

class CashViewModel @Inject constructor(
    private val productUseCase: ProductUseCase,
) : ViewModel() {

    // Estados das entidades que serão observadas pela view
    private var _cash = MutableStateFlow<ViewState<Cash?>?>(null)
    val cash: StateFlow<ViewState<Cash?>?> = _cash

    private var _products = MutableStateFlow<ViewState<List<Product>?>?>(null)
    val products: StateFlow<ViewState<List<Product>?>?> = _products

    private var _spotlights = MutableStateFlow<ViewState<List<Spotlight>?>?>(null)
    val spotlights: StateFlow<ViewState<List<Spotlight>?>?> = _spotlights

    init {
        loadData()
    }

    internal fun loadData() {
        viewModelScope.launch {
            kotlin.runCatching {
                when (val result = productUseCase.getProducts()) {
                    is ViewState.Success -> {
                        val data = result.data
                        _cash.value = ViewState.Success(data?.cash)
                        _products.value = ViewState.Success(data?.products)
                        _spotlights.value = ViewState.Success(data?.spotlight)
                    }
                    is ViewState.Failed -> {
                        _products.value = ViewState.Failed(result.throwable)
                    }
                    else -> {}
                }
            }.onFailure { throwable ->
                _products.value = ViewState.Failed(throwable)
                throwable.printStackTrace()
            }
        }
    }
}
