package br.com.digio.androidtest.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import br.com.digio.androidtest.data.model.Spotlight

@Dao
interface SpotlightDao {
    @Query("SELECT * FROM spotlight")
    suspend fun getSpotlights(): List<Spotlight>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(spotlights: List<Spotlight>)

    @Query("DELETE FROM spotlight")
    fun dropTableSpotlight()
}
