package br.com.digio.androidtest.domain.usecases

import br.com.digio.androidtest.data.repository.DigioRepository
import br.com.digio.androidtest.data.model.DigioProducts
import br.com.digio.androidtest.data.repository.ViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ProductUseCase(private val repository: DigioRepository) {

    suspend fun getProducts(): ViewState<DigioProducts?> {
        return try {
            ViewState.Success(withContext(Dispatchers.IO) { repository.getProducts() })
        } catch (e: Exception) {
            ViewState.Failed(e)
        }
    }
}
