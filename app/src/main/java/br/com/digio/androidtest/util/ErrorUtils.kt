package br.com.digio.androidtest.util

import android.content.Context
import br.com.digio.androidtest.R
import br.com.digio.androidtest.data.repository.ErrorType

object ErrorUtils {
    fun handleError(context: Context?, errorType: ErrorType?): String {
        return when (errorType) {
            ErrorType.NETWORK_ERROR -> context?.getString(R.string.error_network) ?: ""
            ErrorType.NO_DATA_FOUND -> context?.getString(R.string.error_no_data) ?: ""
            ErrorType.GENERIC_ERROR -> context?.getString(R.string.error_generic) ?: ""
            else -> ""
        }
    }
}
