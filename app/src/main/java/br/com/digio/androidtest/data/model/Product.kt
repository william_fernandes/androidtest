package br.com.digio.androidtest.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product")
data class Product(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    @ColumnInfo(name = "image_url") var imageURL: String? = null,
    @ColumnInfo(name = "name") var name: String? = null,
    @ColumnInfo(name = "description") var description: String? = null,
) {
    constructor() : this(0, null, null, null)
}
