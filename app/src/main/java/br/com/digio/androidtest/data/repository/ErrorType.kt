package br.com.digio.androidtest.data.repository

enum class ErrorType {
    NETWORK_ERROR,
    NO_DATA_FOUND,
    GENERIC_ERROR,
}
