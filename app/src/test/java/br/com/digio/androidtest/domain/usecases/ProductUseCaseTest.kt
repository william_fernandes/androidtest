package br.com.digio.androidtest.domain.usecases

import br.com.digio.androidtest.data.model.Cash
import br.com.digio.androidtest.data.model.DigioProducts
import br.com.digio.androidtest.data.repository.DigioRepository
import br.com.digio.androidtest.data.repository.ViewState
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ProductUseCaseTest {

    @Mock
    private lateinit var repository: DigioRepository

    private lateinit var useCase: ProductUseCase

    @Before
    fun setup() {
        useCase = ProductUseCase(repository)
    }

    @Test
    fun `getProducts should return success view state`() = runBlocking {
        val digioProducts = DigioProducts(Cash("", ""), listOf(), listOf())
        val expectedViewState = ViewState.Success(digioProducts)

        `when`(repository.getProducts()).thenReturn(digioProducts)

        val actualViewState = useCase.getProducts()

        assertEquals(expectedViewState, actualViewState)
    }
}
