package br.com.digio.androidtest.data

import br.com.digio.androidtest.data.local.dao.ProductDao
import br.com.digio.androidtest.data.local.dao.SpotlightDao
import br.com.digio.androidtest.data.model.DigioProducts
import br.com.digio.androidtest.data.remote.DigioEndpoint
import br.com.digio.androidtest.util.TestUtil
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.* // ktlint-disable no-wildcard-imports
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DigioRepositoryImplTest {

    @Test
    fun `test get products returns cached data when endpoint fails`() = runBlocking {
        val endpoint = mock(DigioEndpoint::class.java)
        val productDao = mock(ProductDao::class.java)
        val spotlightDao = mock(SpotlightDao::class.java)
        val repository = DigioRepositoryImpl(endpoint, productDao, spotlightDao)

        val data = TestUtil.mockDigioProducts()
        val digioProducts = DigioProducts(data.cash, data.products, data.spotlight)

        `when`(productDao.getProducts()).thenReturn(digioProducts.products)
        `when`(spotlightDao.getSpotlights()).thenReturn(digioProducts.spotlight)

        val result = repository.getProducts()

        assertEquals(result?.cash?.title, "")
        assertEquals(digioProducts.products, result?.products)
        assertEquals(digioProducts.spotlight, result?.spotlight)
    }

    @Test
    fun `test save to database inserts data into database`() = runBlocking {
        val endpoint = mock(DigioEndpoint::class.java)
        val productDao = mock(ProductDao::class.java)
        val spotlightDao = mock(SpotlightDao::class.java)
        val repository = DigioRepositoryImpl(endpoint, productDao, spotlightDao)

        val data = TestUtil.mockDigioProducts()
        val digioProducts = DigioProducts(data.cash, data.products, data.spotlight)

        repository.saveToDatabase(digioProducts)

        verify(productDao).insertAll(data.products)
        verify(spotlightDao).insertAll(data.spotlight)
    }
}
