package br.com.digio.androidtest.data.repository

import br.com.digio.androidtest.data.DigioRepositoryImpl
import br.com.digio.androidtest.data.local.dao.ProductDao
import br.com.digio.androidtest.data.local.dao.SpotlightDao
import br.com.digio.androidtest.data.remote.DigioEndpoint
import br.com.digio.androidtest.util.TestUtil
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DigioRepositoryTest {

    @Test
    fun `test save to database saves data correctly`() = runBlocking {
        val mockEndpoint = mock(DigioEndpoint::class.java)
        val mockProductDao = mock(ProductDao::class.java)
        val mockSpotlightDao = mock(SpotlightDao::class.java)
        val repository = DigioRepositoryImpl(mockEndpoint, mockProductDao, mockSpotlightDao)

        val data = TestUtil.mockDigioProducts()
        repository.saveToDatabase(data)

        verify(mockProductDao).insertAll(data.products)
        verify(mockSpotlightDao).insertAll(data.spotlight)
    }
}