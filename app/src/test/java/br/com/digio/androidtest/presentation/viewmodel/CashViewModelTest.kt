package br.com.digio.androidtest.presentation.viewmodel

import br.com.digio.androidtest.data.model.Cash
import br.com.digio.androidtest.data.model.DigioProducts
import br.com.digio.androidtest.data.model.Product
import br.com.digio.androidtest.data.model.Spotlight
import br.com.digio.androidtest.data.repository.ViewState
import br.com.digio.androidtest.domain.usecases.ProductUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CashViewModelTest {

    @Mock
    private lateinit var mockProductUseCase: ProductUseCase

    private lateinit var cashViewModel: CashViewModel

    @Before
    fun setup() {
        Dispatchers.setMain(TestCoroutineDispatcher())
        cashViewModel = CashViewModel(mockProductUseCase)
    }

    @Test
    fun `loadData should update cash, products and spotlights with ViewState Success`() {
        val expectedCash = Cash("banner_url", "title")
        val expectedProducts = listOf(Product(1, "image_url", "name", "description"))
        val expectedSpotlights = listOf(Spotlight(1, "banner_url", "name", "description"))
        val expectedViewState = ViewState.Success(DigioProducts(expectedCash, expectedProducts, expectedSpotlights))

        runBlocking {
            `when`(mockProductUseCase.getProducts()).thenReturn(expectedViewState)
        }

        cashViewModel.loadData()

        assertEquals(ViewState.Success(expectedViewState.data.cash), cashViewModel.cash.value)
        assertEquals(ViewState.Success(expectedViewState.data.products), cashViewModel.products.value)
        assertEquals(ViewState.Success(expectedViewState.data.spotlight), cashViewModel.spotlights.value)
    }

    @Test
    fun `loadData should update products with ViewState Failed`() {
        val expectedViewState = ViewState.Failed(Exception("Test exception"))

        runBlocking {
            `when`(mockProductUseCase.getProducts()).thenReturn(expectedViewState)
        }

        cashViewModel.loadData()

        assertNull(cashViewModel.cash.value)
        assertEquals(ViewState.Failed(expectedViewState.throwable), cashViewModel.products.value)
        assertNull(cashViewModel.spotlights.value)
    }
}
