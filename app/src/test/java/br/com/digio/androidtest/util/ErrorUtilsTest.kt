import android.content.Context
import br.com.digio.androidtest.R
import br.com.digio.androidtest.data.repository.ErrorType
import br.com.digio.androidtest.util.ErrorUtils
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.Mockito.* // ktlint-disable no-wildcard-imports
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class ErrorUtilsTest {

    @Mock
    private lateinit var context: Context

    @Test
    fun `test handleError with NETWORK_ERROR`() {
        `when`(context.getString(R.string.error_network)).thenReturn(
            "Você está sem conexão com a internet. O aplicativo está em modo offline.",
        )

        ErrorUtils.handleError(context, ErrorType.NETWORK_ERROR)

        verify(context, times(1)).getString(R.string.error_network)
    }

    @Test
    fun `test handleError with NO_DATA_FOUND`() {
        `when`(context.getString(anyInt())).thenReturn(
            "Não foi possível obter os dados. Tente novamente mais tarde.",
        )

        ErrorUtils.handleError(context, ErrorType.NO_DATA_FOUND)

        verify(context).getString(R.string.error_no_data)
    }

    @Test
    fun `test handleError with GENERIC_ERROR`() {
        `when`(context.getString(anyInt())).thenReturn(
            "Ocorreu um erro. Tente novamente mais tarde.",
        )

        ErrorUtils.handleError(context, ErrorType.GENERIC_ERROR)

        verify(context).getString(R.string.error_generic)
    }
}
